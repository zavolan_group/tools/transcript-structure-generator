"""Main module."""
from tsg.cli import app

if __name__ == "__main__":
    app()
